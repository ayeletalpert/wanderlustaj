function [traj] = myWanderlust(data,Options)
%% Calculate knn graph for the data:
lnn = parfor_spdists_knngraph( data, Options.l,...
    'distance', Options.metric,...
    'chunk_size', 1000,...
    'verbose', Options.verbose );
%% shared NN: here we prun the edges in the lnn graph that connect 2 nodes
% with less than Options.snn nearest neibours (most likely short circuits)
lnn = correct2snn(lnn, Options, data);
%% remove k edges out of l, and calculate the trajectory in each case:
G = [];
for graph_iter = 1:Options.num_graphs
    iter_t = tic;
    
    % randomly generate a klNN graph
    if (Options.k~=Options.l)
        klnn = spdists_klnn( lnn, Options.k, Options.verbose );
    else
        klnn = lnn;
    end
    
    % make spdists undirected (by duplicating directional edges).
    klnn = spdists_undirected( klnn );
    
    % run traj. landmarks: here the landmarks are defined by random
    % sampling:
    [ traj, dist, iter_l, RNK,paths_l2l, diffdists,Y] = trajectory_landmarks( klnn,data, Options);
    
    % save output variables
    G.landmarks(graph_iter, :) = iter_l;
    G.traj(graph_iter) = {traj};
    G.dist(graph_iter) = {dist};
    G.klnn(graph_iter) = {klnn};
    
    % calculate weighting matrix, according to the voting_scheme: the
    % distance of the close-to-waypoint points is exponentially
    % proportional to their weight - the closer the way point to the point,
    % the bigger wieght it gets.
    %in case of branching, there is more code here:
    W = calcWeightingMatrix(data, dist, Options);
    
    %iterate the positions of the landmarks until convergence:
    
    % save initial solution - start point's shortest path distances
    % t is the weighted score in the trajectory for all cells in each iteration
    t=[];
    t( 1,:)  = traj(1,:);
    t( end+1, : ) = sum( traj .* W );
    
    % iteratively realign trajectory (because landmarks moved)
    converged = 0; user_break = 0; realign_iter = 2;
    
    while  ~converged && ~user_break
        realign_iter = realign_iter + 1;
        
        traj = dist;
        for idx = 1:size( dist, 1 )
            % find position of landmark in previous iteration
            idx_val = t( realign_iter - 1, iter_l( idx ) );
            % convert all cells before starting point to the negative
            before_indices = find( t( realign_iter - 1, : ) < idx_val );
            traj( idx, before_indices ) = -dist( idx, before_indices );
            % set zero to position of starting point
            traj( idx, : ) = traj( idx, : ) + idx_val;
        end
        
        if (Options.branch)
            [RNK, bp, diffdists, Y] = splittobranches(traj, traj(1, : ),data, iter_l, dist,paths_l2l, G.Opts);
            W = muteCrossBranchVoting(W_full, RNK, RNK(G.Opts.s), iter_l,Y);
        end
        
        % calculate weighed trajectory
        t( realign_iter, : ) = sum( traj .* W );
        
        % check for convergence
        fpoint_corr = corr( t( realign_iter, : )', t( realign_iter - 1, : )' );
        fprintf( 1, '%2.5f...', fpoint_corr);
        converged = fpoint_corr > 0.9999;
        
        if (mod(realign_iter,16)==0)
            % break after too many alignments - something is wrong
            user_break = true;
            fprintf('\nWarning: Force exit after %g iterations\n', realign_iter);
        end
    end
    
    % save final trajectory for this graph
    G.T(graph_iter, :) = t(realign_iter, :);
    
    if ~isempty(Options.exclude_points)
        nearest_landmarks = knnsearch(data(iter_l, :), data(G.Opts.exclude_points, :));
        G.T(graph_iter, G.Opts.exclude_points) = G.T(graph_iter, iter_l(nearest_landmarks));
    end
    
    if (Options.branch)
        bp=0;
        G.B(graph_iter, :) = RNK;
        G.diffdists = diffdists;
        G.bp(graph_iter) = bp;
        G.Y(graph_iter, :) = real(Y);
    else
        G.B = G.T; % branch
        G.bp(graph_iter) = 0;
    end
    
    if( Options.verbose )
        toc( iter_t );
        
        fprintf( 1, '\n' );
    end
end
%% average the values of the events resulted from each trajectory.
traj = mean(G.T, 1);