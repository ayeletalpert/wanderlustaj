% [ traj, dist, l ] = trajectory_landmarks( spdists, s, n, verbose )
%
% calculate the trajectory score of each point in spdists.
%
% s: list of indices of possible starting points. one of these points will be used to generate a reference
% trajectory; the landmark shortest paths will be aligned to this reference.
% n: list of landmark indices to use; or, alternatively, the number of landmarks to choose randomly from all
% points.
%
% traj is a |n|x|spdists| matrix, where row i is the aligned shortest path from landmark i to each other point.
% dist is a |n|x|spdists| matrix, where row i is the shortest path from landmark i to each other point. l is
% the list of landmarks, l(1) is the starting point.
function [ traj, dist, l, RNK,paths_l2l, diffdists,Y ] = trajectory_landmarks( spdists,data, Options)

    Y = [];
    RNK = zeros(size(data, 1), 1);
    n = Options.num_landmarks;

    %find the shortest path from the starting point to each point in the
    %data:
	if( length( n ) == 1 )
        [dists, paths, ~] = graphshortestpath( spdists, Options.s,'METHOD','Dijkstra', 'directed', true);
        
        % if not given landmarks list, decide on random landmarks
        n_opts = 1:size(data,1);
        n = randsample( n_opts, n - 1);
        
        % flock landmarks (landmarks optimization): for each landmark, find its 20 closest NN, then
        % set the observation that is closest to their median as a landmark
        if (Options.flock_landmarks > 0)
            for k=1:Options.flock_landmarks
                [IDX, ~] = knnsearch(data, data(n, :), 'distance', Options.metric, 'K', 20);
                for i=1:numel(n)
                    n(i) = knnsearch(data, median(data(IDX(i, :), :)), 'distance', Options.metric);
                end
            end
        end
    end

    diffdists = zeros(length(n), length(n));

    partial_order = [Options.s;Options.partial_order(:)]; % partial_order includes start point
	l = [ partial_order; n(:) ]; % add extra landmarks if user specified
    
    % calculate the shortest paths from each way-point to the others, in
    % case there are isolated way-points (no shortest path between the
    % source tot he target), find the closest point to the source.
    paths_l2l = cell(length(l),1);
    for li = 1:length( l )
        [dist( li, : ), paths, ~] = graphshortestpath( spdists, l( li ),'METHOD','Dijkstra', 'directed', false );
        if sum(cellfun(@(x)isempty(x), paths(l))) 
            fprintf('\nWarning: found empty path');
        end
        paths_l2l(li) = {paths(l)};
        unreachable = (dist(li,:)==inf);
        unreachable(Options.exclude_points) = 0;

        while (any(unreachable) && Options.search_connected_components)
            fprintf(['\n Warning: %g were unreachable. try increasing l'...
                'or k.Your data is possibly non continous, ie '...
                'has a completely separate cluster of points.'...
                'Wanderlust will roughly estimate their distance for now \n'],...
                sum(unreachable));
            % find closest unreachable point to reachable points.
            % connect it on the spdists. continue iteratively.
            unreachablei = find(unreachable);
            reachablei = find(~unreachable);
            cou = 0;
            while ~isempty(unreachablei)
                cou = cou+1;
                [idx, d] = knnsearch(data(unreachablei, :), data(reachablei, :));
                closest_reachable = d==min(d);
                
                %add connection to spdists
                spdists(reachablei(closest_reachable),...
                    unreachablei(idx(closest_reachable))) = min(d);
                spdists(unreachablei(idx(closest_reachable)),...
                    reachablei(closest_reachable)) = min(d);
                % move points from unreachable list to reachable
                reachablei(end+1:end+length(find(closest_reachable))) = ...
                    unreachablei(idx(closest_reachable));
                unreachablei(idx(closest_reachable)) = [];
                
                if ~mod(cou, 10)
                    break;
                end
            end
            [dist( li, : ), paths, ~] = graphshortestpath( spdists, l( li ),'METHOD','Dijkstra', 'directed', false );
            paths_l2l(li) = {paths(l)};
            unreachable = (dist(li,:)==inf);
        end
        
        if(Options.verbose )
            fprintf( 1, '.' );
        end
    end
    if ~isempty(Options.exclude_points)
        dist(:, Options.exclude_points) = mean(mean(dist~=inf));
    end
    
    if any(any(dist==inf))
        dist(dist==inf) = max(max(dist~=inf));
        if (Options.verbose)
            fprintf('\nwarning: some points remained unreachable (dist==inf)');
        end
    end
    
    % adjust paths according to partial order by redirecting
    nPartialOrder = length(partial_order);
    for radius = 1:nPartialOrder 
        for landmark_row = 1:nPartialOrder
            if (landmark_row + radius <= nPartialOrder)
                a = landmark_row;
                b = landmark_row + (radius-1);
                c = landmark_row + radius;
                dist(a, partial_order(c)) = dist(a, partial_order(b)) + dist(b, partial_order(c));
            end
            if (landmark_row - radius >= 1)
                a = landmark_row;
                b = landmark_row - (radius-1);
                c = landmark_row - radius;
                dist(a, partial_order(c)) = dist(a, partial_order(b)) + dist(b, partial_order(c));
            end
        end
    end

	% align to dist_1 - this for loop refers to partial order stuff
	traj = dist;
    for idx = 2:length(partial_order)
        [~, closest_landmark_row] = min(dist); %closest landmark will determine directionality
        traj(idx, closest_landmark_row < idx) = -dist(idx, closest_landmark_row < idx);
        traj( idx, : ) = traj( idx, : ) + dist( 1, l( idx ) );
    end
    
    % This is the actual align for regular wanderlust:
    %alignment to the source point: for each waypoint, for each other
    %point, its difference from the source point is set to the distance
    %the way point + the distance of the way point from the source point.
    if length( l ) > length(partial_order)
        for idx = length(partial_order)+1:length( l )
            % find position of landmark in dist_1
            idx_val = dist( 1, l( idx ) );
            % convert all cells before starting point to the negative
            before_indices = find( dist( 1, : ) < idx_val );
            traj( idx, before_indices ) = -dist( idx, before_indices );
            % set zero to position of starting point
            traj( idx, : ) = traj( idx, : ) + idx_val;
        end
    end

    
    if (Options.branch)
        [RNK, bp, diffdists, Y] = splittobranches(traj, traj(1, :), data, l, dist, paths_l2l, Options);
    end
end
