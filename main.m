%% read data:
dataDir = 'C:\Users\Administrator\Documents\MDPhD\AML\CD45midHighFiles\isolatedAML';
fileName = '1_1421_AML.fcs';
[fcsdat, fcshdr] = fca_readfcs([dataDir,'\',fileName]);
%% arcsinh transformation to all data:
fcsdat = arcsinhTrans( fcsdat, 5 );
%% get the wanderlust columns:
trajMarkers = {'CD38','CXCR4','CD61','CD34','CD9','CD123','CD33','CD45RA','CD41','CD64','CD11c','CD14','CD13','CD117','CD11b','HLADR','CD45'};
trajColumns = find(ismember(char(fcshdr.par.name2), trajMarkers));
data = fcsdat(:, trajColumns);
%% initialization of the Wanderlust parameters:
Options = [];
Options.branch = 0; 
Options.band_sample = 0;
Options.voting_scheme = 'Exponential';
Options.flock_landmarks = 2; %for landmarks optimization
Options.snn = 1; %how many shared nn should be in the lnn graph
Options.l = 30;
Options.k = 8;
Options.num_landmarks = 20;
Options.num_graphs = 10;
Options.metric = 'Euclidean';
Options.selected_gate = 2; %enter the ordinal number of the gate here
Options.normalization = 'None';
Options.kEigs = 10;
Options.verbose= 1; %communication with the user
Options.partial_order = []; %user-specified way-points; used by the function: "trajectory landmarks"
Options.search_connected_components = 1; %for isolated waypoints, search their closest neigbour
Options.exclude_points = [];
Options.s = 1903;
%% run my Wanderlust:
traj = myWanderlust(data, Options);
