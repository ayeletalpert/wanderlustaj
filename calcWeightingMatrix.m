function W_full = calcWeightingMatrix(data, dist, Options)

if strcmpi(Options.voting_scheme, 'uniform')
    W_full(:, :) = ones(numel(iter_l), size(data,1));
elseif strcmpi(Options.voting_scheme, 'exponential')
    sdv = mean ( std ( dist) )*3;
    W_full = exp( -.5 * (dist / sdv).^2);
elseif strcmpi(Options.voting_scheme, 'linear')
    W_full = repmat(max(dist), size( dist, 1 ), 1) - dist;
    if ~isempty(Options.exclude_points)
        W_full(:, Options.exclude_points) = 1;
    end
elseif strcmpi(Options.voting_scheme, 'quadratic')
    W_full = repmat(max(dist.^2), size( dist, 1 ), 1) - dist.^2;
end

% The weghing matrix must be a column stochastic operator
% some kind of normalization to the weighting matrix
W_full = W_full ./ repmat( sum( W_full ), size( W_full, 1 ), 1 );
