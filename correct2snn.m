function [lnn] = correct2snn(lnn, Options, data)
if (Options.snn ~=0)
    if (Options.verbose), fprintf('updating using jaccard...\n'); tic; end
    
    [j, i, s] = find(lnn);
    % observational note: i is sorted with l-1 apearences each index
    % use this irreliable observation to make sn faster
    
    nData = size(data,1);
    rem = cell(1, nData);
    
    tic;
    % for each point 1..n
    parfor ci=1:nData
        
        % grab neighbors
        from = (ci-1)*Options.l+1;
        to = ci*Options.l;
        i_inds = from:to;
        i_neighs = j(i_inds);
        
        % for each neighbor
        for i_ind=i_inds
            i_neigh=j(i_ind);
            
            % grab the neighbor's neighbors
            from = (i_neigh-1)*Options.l+1;
            to = i_neigh*Options.l;
            j_neighs = j(from:to);
            
            % if the shared neighbors are not many enough
            if sum(ismember(i_neighs, j_neighs)) < Options.snn
                
                % add them to remove list
                rem{ci} = [rem{ci} i_ind];
            end
        end
    end
    
    rem = cell2mat(rem);
    
    % remove relevant indices
    i(rem) = [];
    j(rem) = [];
    s(rem) = [];
    lnn = sparse(j, i, s);
    
    if Options.verbose, fprintf('jaccard computed: %gs\n', toc); end
end