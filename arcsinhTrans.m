function [ transData ] = arcsinhTrans( fcsdat, cofactor )
% arcsinh transformation of the data-set, with cofactor supplied by the
% user:

transData = zeros(size(fcsdat));
for i=1:size(fcsdat,1)
    for j=1:size(fcsdat,2)
        transData(i,j) = asinh(fcsdat(i,j)/cofactor);
    end
end

end

